<?php

//fonction de connexion pdo à la base de données back_chaussure
function connexionBDD() 
{
    try {
        $bdd = new PDO('mysql:host=localhost;dbname=back_chaussure', 
        'telemaque','Ch@ussure24');

        $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $bdd;
    }
    
    catch(Exception $e) {
        die('Erreur : '.$e->getMessage());
    }
}

//fonction pour récupérer toutes les chaussures de la BDD
function afficherChaussures() 
{
    $connexion = connexionBDD();

    //requete SQL select qui récupère toutes les chaussures de la table CHAUSSURE
    $requeteChaussures = "SELECT id, marque, type, matiere, couleur, prix, 
    dateRayon FROM CHAUSSURE";
    $resultatChaussures = $connexion->query($requeteChaussures);
    
    $tabIdChaussure = array();
    $tabMarque = array();
    $tabType = array();
    $tabMatiere = array();
    $tabCouleur = array();
    $tabPrix = array();
    $tabDateRayon = array();

    //on prend des tableaux de chaque champs de la table chaussure
    foreach($resultatChaussures as $row) {
        array_push($tabIdChaussure, $row['id']);
        array_push($tabMarque, $row['marque']);
        array_push($tabType, $row['type']);
        array_push($tabMatiere, $row['matiere']);
        array_push($tabCouleur, $row['couleur']);
        array_push($tabPrix, $row['prix']);
        array_push($tabDateRayon, $row['dateRayon']);
    }

    $tabChaussures = array();
    array_push($tabChaussures, $tabIdChaussure, $tabMarque, $tabType, $tabMatiere, 
    $tabCouleur, $tabPrix, $tabDateRayon);

    return $tabChaussures;
}

//fonction qui recupere toutes les tailles de chaussure disponible pour la chaussure d'id en paramètre
function afficherTaillesChaussure($idChaussure) 
{
    $connexion = connexionBDD();

    $requeteTailles = "SELECT nom FROM TAILLE JOIN POSSEDE ON TAILLE.id 
    = POSSEDE.id_taille WHERE id_chaussure = ".$idChaussure." AND nombre > 0";

    $resultatTailles = $connexion->query($requeteTailles);
    
    $tabTailles = array();

    //on récupère un tableau de nom de tailles
    foreach($resultatTailles as $row) {
        array_push($tabTailles, $row['nom']);
    }

    return $tabTailles;
}

//fonction pour supprimer une chaussure avec en paramètre son id
function supChaussure($idChaussure)
{
    $connexion = connexionBDD();

    //suppression de la chaussure dans sa table CHAUSSURE
    $requeteDeleteChaussure = $connexion->exec("DELETE FROM CHAUSSURE WHERE id = ".$idChaussure);

    //suppression de la chaussure dans sa table POSSEDE
    $requeteDeleteChaussurePossede = $connexion->exec("DELETE FROM POSSEDE WHERE id_chaussure = ".$idChaussure);

    header('Location: index.php');
}

//fonction qui permet d'ajouter une chaussure et ses tailles dans les 3 tables necessaires
function ajoutChaussure($marque, $type, $matiere, $couleur, $prix, $dateRayon, $tabTaillesNew) 
{
    $connexion = connexionBDD();

    $requeteAjoutChaussure = $connexion->prepare("INSERT INTO CHAUSSURE (marque, type, matiere,
    couleur, prix, dateRayon)
    VALUES (:marque, :type, :matiere, :couleur, :prix, :dateRayon)");
    
    $requeteAjoutChaussure->execute(
        array(
            "marque" => $marque,
            "type" => $type,
            "matiere" => $matiere,
            "couleur" => $couleur,
            "prix" => $prix,
            "dateRayon" => $dateRayon,
        )
    );
    
    //récupération de l'id chaussure qu'on vient d'insérer
    $idChaussure = $connexion->lastInsertId();
    $tabTaillesOld = afficherTaillesChaussure($idChaussure);
    
    //TAILLES A INSERT
    $taillesInsert = array_diff($tabTaillesNew, $tabTaillesOld);
    foreach ($taillesInsert as $row) {
        //pour vérifier si il y déjà cette taille dans la table taille
        $requeteNomTaille = "SELECT nom FROM TAILLE WHERE nom = '".$row."'";
        
        $resultatNomTaille = $connexion->query($requeteNomTaille);
        $resultatNomTaille = $resultatNomTaille->fetch();
        
        //si cette taille n'existe pas dans la table TAILLE
        if (is_null($resultatNomTaille[0])) {

            //INSERT taille dans table TAILLE
            $requeteInsertTaille = $connexion->prepare("INSERT INTO TAILLE (nom) VALUES (?)");

            $requeteInsertTaille->execute([$row]);
        }

        //pour récupérer l'id taille de la taille qu'on est entrain d'ajouter
        $requeteIdTaille = "SELECT id FROM TAILLE WHERE nom = '".$row."'";
        $resultatIdTaille = $connexion->query($requeteIdTaille);
        $resultatIdTaille = $resultatIdTaille->fetch();

        $requeteVerifExistance = "SELECT id_taille FROM POSSEDE WHERE id_taille = '".$resultatIdTaille[0]."' 
        AND id_chaussure = '".$idChaussure."'";
        $resultatVerifExistance = $connexion->query($requeteVerifExistance);
        $resultatVerifExistance = $resultatVerifExistance->fetch();

        //si cette taille/chaussure n'existe pas dans possede
        if (is_null($resultatVerifExistance[0])) {
            //insert dans l'entité association possede 
            $requeteInsertTaillePoss = $connexion->prepare("INSERT INTO POSSEDE (id_taille, id_chaussure, nombre)
            VALUES (:id_taille, :id_chaussure, :nombre)");

            //j'ai pas géré le nombre de tailles disponibles faute de temps dont je mets 1 par défaut
            $requeteInsertTaillePoss->execute(
                array(
                    "id_taille" => $resultatIdTaille[0],
                    "id_chaussure" => $idChaussure,
                    "nombre" => 1
                )
            );
        }
    }
    header('Location: index.php');
}

//fonction qui permet d'editer une chaussure et ses tailles dans les 3 tables necessaires
function editChaussure($idChaussure, $marque, $type, $matiere, $couleur, $prix, $dateRayon, $tabTaillesNew) 
{
    $connexion = connexionBDD();

    //update de la table chaussure 
    $requeteEditChaussure = $connexion->prepare("UPDATE CHAUSSURE SET marque = '".$marque."', type = '".$type."',
    matiere = '".$matiere."', couleur = '".$couleur."', prix = '".$prix."', dateRayon = '".$dateRayon."' 
    WHERE id = ".$idChaussure);
    
    $requeteEditChaussure->execute();
    
    //récupération des tailles de chaussure dans la BDD
    $tabTaillesOld = afficherTaillesChaussure($idChaussure);
    
    //on compare les différences entre les tailles de chaussure de la BDD et celles en parametre de cette fonction
    $taillesInsert = array_diff($tabTaillesNew, $tabTaillesOld);
    //TAILLES A INSERT
    foreach ($taillesInsert as $row) {
        //pour vérifier si il y déjà cette taille dans la table taille
        $requeteNomTaille = "SELECT nom FROM TAILLE WHERE nom = '".$row."'";
        
        $resultatNomTaille = $connexion->query($requeteNomTaille);
        $resultatNomTaille = $resultatNomTaille->fetch();
        
        //si cette taille n'existe pas dans la table TAILLE
        if (is_null($resultatNomTaille[0])) {

            //INSERT taille dans table TAILLE
            $requeteInsertTaille = $connexion->prepare("INSERT INTO TAILLE (nom) VALUES (?)");

            $requeteInsertTaille->execute([$row]);
        }

        //pour récupérer l'id taille de la taille qu'on est entrain d'ajouter
        $requeteIdTaille = "SELECT id FROM TAILLE WHERE nom = '".$row."'";
        $resultatIdTaille = $connexion->query($requeteIdTaille);
        $resultatIdTaille = $resultatIdTaille->fetch();

        $requeteVerifExistance = "SELECT id_taille FROM POSSEDE WHERE id_taille = '".$resultatIdTaille[0]."' 
        AND id_chaussure = '".$idChaussure."'";
        $resultatVerifExistance = $connexion->query($requeteVerifExistance);
        $resultatVerifExistance = $resultatVerifExistance->fetch();

        //si cette taille/chaussure n'existe pas dans possede
        if (is_null($resultatVerifExistance[0])) {
            //insert dans l'entité association possede 
            $requeteInsertTaillePoss = $connexion->prepare("INSERT INTO POSSEDE (id_taille, id_chaussure, nombre)
            VALUES (:id_taille, :id_chaussure, :nombre)");

            //j'ai pas géré le nombre de tailles disponibles faute de temps dont je mets 1 par défaut
            $requeteInsertTaillePoss->execute(
                array(
                    "id_taille" => $resultatIdTaille[0],
                    "id_chaussure" => $idChaussure,
                    "nombre" => 1
                )
            );
        }
    }
    
    //Tailles A DELETE
    $taillesDelete = array_diff($tabTaillesOld, $tabTaillesNew);

    foreach ($taillesDelete as $row) {
        //pour récupérer l'id taille de la taille qu'on est entrain de supprimer
        $requeteIdTaille = "SELECT id FROM TAILLE WHERE nom = '".$row."'";
        $resultatIdTaille = $connexion->query($requeteIdTaille);
        $resultatIdTaille = $resultatIdTaille->fetch();

        $requeteDeleteTaille = $connexion->exec("DELETE FROM POSSEDE WHERE id_chaussure ='".$idChaussure."' 
        AND id_taille = '".$resultatIdTaille[0]."'");
    }
    header('Location: index.php');
}

