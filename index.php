<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <title>Accueil</title>

    <!-- GOOGLE FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500|Poppins:400,500,600,700|Roboto:400,500" rel="stylesheet"/>
    <link href="https://cdn.materialdesignicons.com/3.0.39/css/materialdesignicons.min.css" rel="stylesheet" />

    <!-- PLUGINS CSS STYLE -->
    <link href="assets/plugins/toaster/toastr.min.css" rel="stylesheet" />
    <link href="assets/plugins/nprogress/nprogress.css" rel="stylesheet" />
    <link href="assets/plugins/flag-icons/css/flag-icon.min.css" rel="stylesheet"/>
    <link href="assets/plugins/jvectormap/jquery-jvectormap-2.0.3.css" rel="stylesheet" />
    <link href="assets/plugins/ladda/ladda.min.css" rel="stylesheet" />
    <link href="assets/plugins/select2/css/select2.min.css" rel="stylesheet" />
    <link href="assets/plugins/daterangepicker/daterangepicker.css" rel="stylesheet" />

    <!-- SLEEK CSS -->
    <link id="sleek-css" rel="stylesheet" href="assets/css/sleek.css" />

    

    <!-- FAVICON -->
    <link href="assets/img/favicon.png" rel="shortcut icon" />

    <script src="assets/plugins/nprogress/nprogress.js"></script>
</head>


  <body class="sidebar-fixed sidebar-dark header-light header-fixed" id="body">

    <?php include 'fonctions.php' ?>

    <script>
      NProgress.configure({ showSpinner: false });
      NProgress.start();
    </script>

    <div class="mobile-sticky-body-overlay"></div>

    <div class="wrapper">
      
              <!--
          ====================================
          ——— LEFT SIDEBAR WITH FOOTER
          =====================================
        -->
      <?php include 'navbar.php'; ?>
      

      <div class="page-wrapper">
        <!-- Header -->
        <header class="main-header " id="header">
          <nav class="navbar navbar-static-top navbar-expand-lg">
            <!-- Sidebar toggle button -->
            <button id="sidebar-toggler" class="sidebar-toggle">
              <span class="sr-only">Toggle navigation</span>
            </button>
          </nav>
        </header>


        <div class="content-wrapper">
          <div class="content">						 
						<div class="row">
							<div class="col-12"> 
                  <!-- Recent Order Table -->
                  <div class="card card-table-border-none" id="recent-orders">
                    <div class="card-header justify-content-between">
                      <h2>Les chaussures</h2>
    
                    </div>
                    <div class="card-body pt-0 pb-5">
                      <table class="table card-table table-responsive table-responsive-large" style="width:100%">
                        <thead>
                          <tr>
                            <th>ID</th>
                            <th>Marque</th>
                            <th>type</th>
                            <th>Matière</th>
                            <th>Couleur</th>
                            <th>Prix</th>
                            <th>Mise en rayon</th>
                            <th>Tailles disponibles</th> <!-- tableau d'une autre requête -->
                            <th><a href="ajoutChaussure.php">Ajouter</a></th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                          //récupération de la liste des chaussures
                          $tabChaussures = afficherChaussures();
                          //pour chaque chaussure on creer une ligne
                          for ($i = 0; $i < sizeof($tabChaussures[0]); $i++) {
                            //récupération de toutes les tailles pour la chaussure avec pour id celui en paramètre
                            $tabTailles = afficherTaillesChaussure($tabChaussures[0][$i]);
                            ?>
                            <tr>
                              <td>
                                <!-- id -->
                                <?php echo $tabChaussures[0][$i]; ?>
                                
                              </td>
                              <td>
                                <!-- marque -->
                                <?php echo $tabChaussures[1][$i]; ?>
                              </td>
                              <td>
                                <!-- type -->
                                <?php echo $tabChaussures[2][$i]; ?>
                              </td>
                              <td>
                                <!-- matiere -->
                                <?php echo $tabChaussures[3][$i]; ?>
                              </td>
                              <td>
                                <!-- couleur -->
                                <?php echo $tabChaussures[4][$i]; ?>
                              </td>
                              <td>
                                <!-- prix -->
                                <?php echo $tabChaussures[5][$i]."€"; ?>
                              </td>
                              <td>
                                <!-- date de mise en rayon -->
                                <?php echo $tabChaussures[6][$i]; ?>
                              </td>
                              <td>
                                <div class="form-group">
                                  <select class="form-control" id="selectTailles">
                                  <?php
                                    $tabTaillesGet = array();
                                    //pour chaque taille on creer des options dans ce select
                                    for ($j = 0; $j < sizeof($tabTailles); $j++) {
                                      echo "<option>".$tabTailles[$j]."</option>";
                                      array_push($tabTaillesGet, $tabTailles[$j]);
                                    }
                                    ?>
                                  </select>
                                </div>   
                              </td>
                              <td class="text-right">
                                <div class="dropdown show d-inline-block widget-dropdown">
                                  <a class="dropdown-toggle icon-burger-mini" href="" role="button" id="dropdown-recent-order1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-display="static"></a>
                                  <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown-recent-order1">
                                    <li class="dropdown-item">
                                      <!-- bouton pour modifier la chaussure de cette ligne on passe tous les paramètres en GET -->
                                      <?php echo "<a href='./modifChaussure.php?id=".$tabChaussures[0][$i]."&marque=".$tabChaussures[1][$i]."&type=".$tabChaussures[2][$i]."&matiere=".$tabChaussures[3][$i]."&couleur=".$tabChaussures[4][$i]."&prix=".$tabChaussures[5][$i]."&dateRayon=".$tabChaussures[6][$i]."&".http_build_query(array('taille' => $tabTaillesGet))."'>Modifier</a>"; ?>
                                    </li>
                                    <li class="dropdown-item">
                                      <!-- bouton pour supprimer la chaussure de cette ligne -->
                                      <?php echo "<a href='./supChaussure.php?id=".$tabChaussures[0][$i]."'>Supprimer</a>"; ?>
                                    </li>
                                  </ul>
                                </div>
                              </td>
                            </tr>
                            <?php
                          }
                          ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
						    </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDCn8TFXGg17HAUcNpkwtxxyT9Io9B_NcM" defer></script>
    <script src="assets/plugins/jquery/jquery.min.js"></script>
    <script src="assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="assets/plugins/toaster/toastr.min.js"></script>
    <script src="assets/plugins/slimscrollbar/jquery.slimscroll.min.js"></script>
    <script src="assets/plugins/charts/Chart.min.js"></script>
    <script src="assets/plugins/ladda/spin.min.js"></script>
    <script src="assets/plugins/ladda/ladda.min.js"></script>
    <script src="assets/plugins/jquery-mask-input/jquery.mask.min.js"></script>
    <script src="assets/plugins/select2/js/select2.min.js"></script>
    <script src="assets/plugins/jvectormap/jquery-jvectormap-2.0.3.min.js"></script>
    <script src="assets/plugins/jvectormap/jquery-jvectormap-world-mill.js"></script>
    <script src="assets/plugins/daterangepicker/moment.min.js"></script>
    <script src="assets/plugins/daterangepicker/daterangepicker.js"></script>
    <script src="assets/plugins/jekyll-search.min.js"></script>
    <script src="assets/js/sleek.js"></script>
    <script src="assets/js/chart.js"></script>
    <script src="assets/js/date-range.js"></script>
    <script src="assets/js/map.js"></script>
    <script src="assets/js/custom.js"></script>
  </body>
</html>
