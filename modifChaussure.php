<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <title>Modification de chaussure</title>

    <!-- GOOGLE FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500|Poppins:400,500,600,700|Roboto:400,500" rel="stylesheet"/>
    <link href="https://cdn.materialdesignicons.com/3.0.39/css/materialdesignicons.min.css" rel="stylesheet" />

    <!-- PLUGINS CSS STYLE -->
    <link href="assets/plugins/toaster/toastr.min.css" rel="stylesheet" />
    <link href="assets/plugins/nprogress/nprogress.css" rel="stylesheet" />
    <link href="assets/plugins/flag-icons/css/flag-icon.min.css" rel="stylesheet"/>
    <link href="assets/plugins/jvectormap/jquery-jvectormap-2.0.3.css" rel="stylesheet" />
    <link href="assets/plugins/ladda/ladda.min.css" rel="stylesheet" />
    <link href="assets/plugins/select2/css/select2.min.css" rel="stylesheet" />
    <link href="assets/plugins/daterangepicker/daterangepicker.css" rel="stylesheet" />

    <!-- SLEEK CSS -->
    <link id="sleek-css" rel="stylesheet" href="assets/css/sleek.css" />

    

    <!-- FAVICON -->
    <link href="assets/img/favicon.png" rel="shortcut icon" />

    <script src="assets/plugins/nprogress/nprogress.js"></script>
  </head>


  <body class="sidebar-fixed sidebar-dark header-light header-fixed" id="body">

    <?php include 'fonctions.php' ?>

    <script>
      NProgress.configure({ showSpinner: false });
      NProgress.start();
    </script>

    <div class="mobile-sticky-body-overlay"></div>

    <div class="wrapper">
      
      <!-- NAVBAR INCLUDE -->
      <?php include 'navbar.php'; ?>
      
      <div class="page-wrapper">
        <!-- Header -->
        <header class="main-header " id="header">
          <nav class="navbar navbar-static-top navbar-expand-lg">
            <!-- Sidebar toggle button -->
            <button id="sidebar-toggler" class="sidebar-toggle">
              <span class="sr-only">Toggle navigation</span>
            </button>
          </nav>
        </header>

        <div class="content-wrapper">
          <div class="content">							
            <div class="row">
              <div class="col-lg-6">
                <div class="card card-default">
                  <div class="card-header card-header-border-bottom">
                    <?php $idChaussure = $_GET['id']; ?>
                    <h2>Chaussure avec pour id = <?php echo $idChaussure; ?></h2>
                  </div>
                  <div class="card-body">
                    <?php echo "<form method='post' action='./modifChaussurePost.php?id=".$idChaussure."'>"; ?>
                      <div class="form-group">
                        <label>Marque</label>
                        <input type="text" class="form-control" name="inputMarque" id="inputMarque" placeholder="Marque"
                        value="<?php echo $_GET['marque'];?>">
                      </div>
                      <div class="form-group">
                        <label>Type</label>
                        <input type="text" class="form-control" name="inputType" id="inputType" placeholder="Type"
                        value="<?php echo $_GET['type'];?>">
                      </div>
                      <div class="form-group">
                        <label>Matiere</label>
                        <input type="text" class="form-control" name="inputMatiere" id="inputMatiere" placeholder="Matiere"
                        value="<?php echo $_GET['matiere'];?>">
                      </div>
                      <div class="form-group">
                        <label>Couleur</label>
                        <input type="text" class="form-control" name="inputCouleur" id="inputCouleur" placeholder="Couleur"
                        value="<?php echo $_GET['couleur'];?>">
                      </div>
                      <div class="form-group">
                        <label>Prix</label>
                        <input type="text" class="form-control" name="inputPrix" id="inputPrix" placeholder="Prix"
                        value="<?php echo $_GET['prix'];?>">
                      </div>
                      <div class="form-group">
                        <label>Mise en rayon</label>
                        <input type="text" class="form-control" name="inputRayon" id="inputRayon" placeholder="Date de mise en rayon"
                        value="<?php echo $_GET['dateRayon'];?>">
                      </div>
                      <div class="form-group">
                        <label>Tailles disponibles</label>
                        <select name="selectTailles[]" multiple class="form-control" id="selectTailles">
                          <?php 
                          for ($i=0; $i < sizeof($_GET['taille']); $i++) {
                            echo "<option selected='' value ='".$_GET['taille'][$i]."'>".$_GET['taille'][$i]."</option>";
                          }
                          ?>
                        </select>
                        <br>
                        <label>Ajouter une taille</label>
                        <input type="text" class="form-control" id="inputTailleAjout" placeholder="Nouvelle taille">
                        <input type="button" id="btnAjoutTaille" class="btn btn-primary btn-default"></input>
                        <br>
                        <label>Supprimer une taille</label>
                        <input type="text" class="form-control" id="inputTailleSup" placeholder="Taille a supprimer">
                        <input type="button" id="btnSupTaille" class="btn btn-primary btn-default"></input>
                      </div>
                      <div class="form-footer pt-4 pt-5 mt-4 border-top">
                        <button type="submit" class="btn btn-primary btn-default" onclick="selectAll();">Submit</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>  
          </div>
        </div>  
      </div>
    </div>

    <script src="assets/plugins/jquery/jquery.min.js"></script>

    <!-- script JS pour selectionner tous les elements option de select pour pouvoir tous les post et les récupérer dans un tableau $_POST[''] -->
    <script type="text/javascript">
      function selectAll() 
      { 
        selectBox = document.getElementById("selectTailles");

        for (var i = 0; i < selectBox.options.length; i++) { 
          selectBox.options[i].selected = true; 
        } 
    }
    </script>

    <!-- script JQuery pour ajouter des elements options au select tailles sur pression du bouton -->
    <script> 
      $(function() {
        $('#btnAjoutTaille').click(function() {
          var taille = $('#inputTailleAjout').val();
          $('#selectTailles').append($('<option/>', {
            selected: "",
            value: taille,
            text: taille
          }));
        });
      });
    </script>

    <!-- script JQuery pour supprimer des elements options au select tailles sur pression du bouton -->
    <script> 
      $(function() {
        $('#btnSupTaille').click(function() {
          var taille = $('#inputTailleSup').val();
          $("#selectTailles option[value='"+taille+"']").remove();
        });
      });
    </script>
    
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDCn8TFXGg17HAUcNpkwtxxyT9Io9B_NcM" defer></script>
    
    <script src="assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="assets/plugins/toaster/toastr.min.js"></script>
    <script src="assets/plugins/slimscrollbar/jquery.slimscroll.min.js"></script>
    <script src="assets/plugins/charts/Chart.min.js"></script>
    <script src="assets/plugins/ladda/spin.min.js"></script>
    <script src="assets/plugins/ladda/ladda.min.js"></script>
    <script src="assets/plugins/jquery-mask-input/jquery.mask.min.js"></script>
    <script src="assets/plugins/select2/js/select2.min.js"></script>
    <script src="assets/plugins/jvectormap/jquery-jvectormap-2.0.3.min.js"></script>
    <script src="assets/plugins/jvectormap/jquery-jvectormap-world-mill.js"></script>
    <script src="assets/plugins/daterangepicker/moment.min.js"></script>
    <script src="assets/plugins/daterangepicker/daterangepicker.js"></script>
    <script src="assets/plugins/jekyll-search.min.js"></script>
    <script src="assets/js/sleek.js"></script>
    <script src="assets/js/chart.js"></script>
    <script src="assets/js/date-range.js"></script>
    <script src="assets/js/map.js"></script>
    <script src="assets/js/custom.js"></script>




  </body>
</html>
